<?php
header ('Content-Type: application/json; charset=utf-8');
require_once 'classe/usuario.php';
require_once 'classe/sessao.php';
class Rest
{
  public static function open($requisicao)
  {

    //identifica classe e método na URL
    $url = explode('/',$requisicao['url']);
    $classe = ucfirst($url[0]);
    array_shift($url);

    $metodo =$url[0];
    array_shift ($url);

    //verifica existencia da classe e método
    if (!class_exists($classe))
    {
      return json_encode(array('status'=>'erro','dados'=>'classe inexistente.'));
    }

    if (!method_exists($classe,$metodo))
    {
      return json_encode(array('status'=>'erro','dados'=>'método inexistente.'));

    }

    //não valida a sessão para login e validar login via API
    if ($metodo == "login" || $metodo == "validar_chave"){

      $retorno=call_user_func(array (new $classe,$metodo));
      return json_encode($retorno);
    }
    else {
      $sessao_chave = $url[0];
      array_shift ($url);

      //coleta parametros da URL (where)
      $parametros = array();

      //se logout, passa chave da sessão como parametro para encerrar a sessão pela chave
      if ($metodo == "logout")
      {
        $url[0] = $sessao_chave;
      }

      $parametros = $url;

      //valida chave da sessão
      $chave_valida = false;
      $chave_valida=call_user_func(array("Sessao","validar_chave"),$sessao_chave);

      if ($chave_valida == false){
        return json_encode(array('status'=>'erro','dados'=>'chave inválida/expirada.'));
      }

      $retorno=call_user_func_array(array (new $classe,$metodo),$parametros);
      return json_encode($retorno);
    }
  }
}

if (isset($_REQUEST)) {

    echo Rest::open($_REQUEST);

}
?>
