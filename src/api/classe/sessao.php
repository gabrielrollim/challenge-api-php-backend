<?php

class Sessao
{

  public function login()
  {
      $content = trim (file_get_contents("php://input"));
      $decoded = json_decode($content,true);


      if ($_SERVER['REQUEST_METHOD'] != "POST")
      {
        return array('status'=>'erro','dados'=>'para logar, use o POST');
      }

      if(isset($decoded['usuario_email']) && isset($decoded['usuario_senha']))
      {
          $usuario_email = $decoded['usuario_email'];
          $usuario_senha = $decoded['usuario_senha'];
      }
      else
      {
        return array('status'=>'erro','dados'=>"json incorreto.");
      }

      $conn = mysqli_connect('localhost', 'root', '', 'dbpushstart');
      $sql = "SELECT * FROM usuario WHERE usuario_email='$usuario_email' AND usuario_senha='$usuario_senha' LIMIT 1";
      $resultado = mysqli_query($conn, $sql);

      if (mysqli_num_rows($resultado) > 0){
            $item = mysqli_fetch_assoc($resultado);
            $usuario_id= $item["usuario_id"];

            //chave expira em 5 minutos com a data extraída do banco
            $chave= hash('md5', date('His'));
            $sql = "INSERT INTO `sessao` (`sessao_id`, `usuario_id`, `sessao_chave`, `sessao_dataexpiracao`) VALUES (NULL, $usuario_id, '$chave', NOW() + INTERVAL 5 HOUR_MINUTE)";
            $resultado = mysqli_query($conn, $sql);

            if ($resultado) {

                return array('status'=>'sucesso','dados'=>'login efetuado com sucesso.', 'sessao_chave'=>$chave);
            } else {
                return "Error: " . mysqli_error($conn);
            }
      }
      else {
          return array('status'=>'erro','dados'=>"usuário não encontrado.");
      }

  }

  public function validar_chave($sessao_chave="")
  {

    $chave_valida = false;
    $arrayRetorno= array("status" => "erro" , "mensagem" => "chave de acesso inválida.");
    //identifica se requisição foi via API ou invocação do sistema
    if ($sessao_chave == ""){

      if ($_SERVER['REQUEST_METHOD'] != "POST")
      {
        return array('status'=>'erro','dados'=>'para validar o login, use o POST');
      }
      $content = trim (file_get_contents("php://input"));
      $decoded = json_decode($content,true);
      if(isset($decoded['sessao_chave'])){
          $sessao_chave = $decoded['sessao_chave'];
      }
    }

    $conn = mysqli_connect('localhost', 'root', '', 'dbpushstart');
    $sql = "SELECT * FROM sessao WHERE sessao_chave='$sessao_chave' AND NOW() < sessao_dataexpiracao";
    $resultado = mysqli_query($conn, $sql);

    if (mysqli_num_rows($resultado) > 0){
      $chave_valida = true;
      $arrayRetorno= array("status" => "sucesso" , "mensagem" => "chave de acesso válida.");
    }
    //identifica se requisição foi via API ou invocação do sistema para gerar o retorno
    if(isset($decoded['sessao_chave'])) {
        return $arrayRetorno;
    }
    else{
      return $chave_valida;
    }
  }

  public function logout($sessao_chave="")
  {
    
    if (!$_SERVER['REQUEST_METHOD']=="DELETE")
    {
      return array('status'=>'erro','dados'=>'para logout use o DELETE.');
    }

    if($sessao_chave=="")
    {
        return array('status'=>'erro','dados'=>'informe a chave da sessao.');
    }

    $conn = mysqli_connect('localhost', 'root', '', 'dbpushstart');
    $sql = "UPDATE `sessao` SET `sessao_dataexpiracao` = NOW() WHERE `sessao`.`sessao_chave` = '$sessao_chave';";
    $resultado = mysqli_query($conn, $sql);

    if ($resultado) {
        return array('status'=>'sucesso','dados'=>'sessão encerrada.');
    } else {
        return "Error: " . mysqli_error($conn);
    }
  }
}
?>
