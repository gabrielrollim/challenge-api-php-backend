
<?php
class Usuario
{

  public function consultar($where)
  {
    if ($_SERVER['REQUEST_METHOD'] != "GET")
    {
      return array('status'=>'erro','dados'=>'para consultar , use o GET');
    }

    if($where != ""){
      $where = "where $where";
    }
    $conn = mysqli_connect('localhost', 'root', '', 'dbpushstart');
    $sql = "SELECT * FROM usuario $where ORDER BY usuario_id ASC";
    $resultado = mysqli_query($conn, $sql);
    $arrayRetorno = array();

    if (mysqli_num_rows($resultado) > 0)
    {
      while($item = mysqli_fetch_assoc($resultado))
      {
          $arrayRetorno[] = $item;
      }
    }
    return array('status'=>'sucesso','dados'=>$arrayRetorno);
  }

  public function alterar($usuario_id="")
  {

    $content = trim (file_get_contents("php://input"));
    $decoded = json_decode($content,true);

    if (!$_SERVER['REQUEST_METHOD']=="PUT")
    {
        return array('status'=>'erro','dados'=>'para alterar use o PUT.');
    }

    if ($usuario_id =="")
    {
      return array('status'=>'erro','dados'=>'informe o nome.');
    }
    if(!isset($decoded['usuario_nome']))
    {
      return array('status'=>'erro','dados'=>'informe o nome.');
    }
    if(!isset($decoded['usuario_email']))
    {
      return array('status'=>'erro','dados'=>'informe o email.');
    }
    if(!isset($decoded['usuario_senha']))
    {
      return array('status'=>'erro','dados'=>'informe a senha.');
    }

    $usuario_nome = $decoded["usuario_nome"];
    $usuario_email= $decoded["usuario_email"];
    $usuario_senha = $decoded["usuario_senha"];

    $conn = mysqli_connect('localhost', 'root', '', 'dbpushstart');
    $sql = "UPDATE `usuario` SET `usuario_nome` = '$usuario_nome',`usuario_email`='$usuario_email',`usuario_senha`='$usuario_senha' WHERE `usuario`.`usuario_id` = $usuario_id;";
    $resultado = mysqli_query($conn, $sql);

    if ($resultado) {
        return array('status'=>'sucesso','dados'=>'dados alterados com sucesso.');
    } else {
        return "Error: " . mysqli_error($conn);
    }

  }

  public function alterar_imagem($usuario_id="")
  {


    if ($_SERVER['REQUEST_METHOD'] != "POST")
    {
      return array('status'=>'erro','dados'=>'para alterar a imagem , use o POST');
    }

    if(!isset($_FILES['usuario_imgUrl']))
    {
      return array('status'=>'erro','dados'=>'envie um arquivo.');
    }
    if($usuario_id=="")
    {
      return array('status'=>'erro','dados'=>'informe o id do usuário.');
    }

    $arquivo_tmp = $_FILES['usuario_imgUrl']['tmp_name'];
    $nome = $_FILES['usuario_imgUrl']['name'];
    $usuario_imgUrl = __DIR__."\imagens" . '\\'.$nome;

		$extensao = strrchr($nome, '.');
    $extensao = strtolower($extensao);

    if(!strstr('.jpg;.jpeg;.gif;.png', $extensao))
  	{
      return array('status'=>'erro','dados'=>'arquivo inválido. (permitidos: jpg/jpeg/gif/png)');
    }

		if(move_uploaded_file( $arquivo_tmp, $usuario_imgUrl  ))
		{
      $usuario_imgUrl= str_replace ('\\','\\\\',$usuario_imgUrl);

      $conn = mysqli_connect('localhost', 'root', '', 'dbpushstart');
      $sql = "UPDATE `usuario` SET `usuario_imgUrl` = '$usuario_imgUrl' WHERE `usuario`.`usuario_id` = $usuario_id";

      $resultado = mysqli_query($conn, $sql);

      if ($resultado) {
          return array('status'=>'sucesso','dados'=>'imagem alterada com sucesso.');
      } else {
          return "Error: " . mysqli_error($conn);
      }
		}
  }
}
?>
