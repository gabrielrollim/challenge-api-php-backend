# Challenge - API PHP Backend #


## login ##

identifica o usuário e gera uma chave para controlar a sessão

(POST) http://localhost/pushstart_test/src/api/sessao/login/

Parâmetros:

* usuario_email

* usuario_senha


Exemplo de envio:
{
	"usuario_email":"gabriel.rollim@gmail.com1",
	"usuario_senha":"gabriel1232"
}


Exemplo de retorno:
{
    "status": "sucesso",
    "dados": "login efetuado com sucesso.",
    "sessao_chave": "30f0dde6bb8ce45ab0351d1b40597c4b"
}



--===

## validar chave ##

criada para controle interno e transformada também em endpoint

(POST) http://localhost/pushstart_test/src/api/sessao/validar_chave/

Parâmetros:

* sessao_chave


Exemplo de envio:
{
	"sessao_chave":"40bec476e48e70c565fd7eb9e6b3aff5"
}


Exemplo de retorno:
{
    "status": "sucesso",
    "mensagem": "chave de acesso válida."
}



--===

## consultar ##

consulta usuários cadastrados informando chave de acesso e/ou parametro (where). Se não informado, retorna todos os registros.

(GET) http://localhost/pushstart_test/src/api/usuario/consultar/a789d24568511299309afa236e69c718/usuario_id =5

Parâmetros:

* Chave de acesso

* Cláusula para filtro dos resultados (opcional)


Exemplo de retorno (consulta sem filtro):
{
    "status": "sucesso",
    "dados": [
        {
            "usuario_id": "4",
            "usuario_nome": "Gabriel Rollim",
            "usuario_email": "gabriel.rollim@gmail.com",
            "usuario_senha": "gabriel123",
            "usuario_imgUrl": "E:\\Pushstart\\pushstart_test\\src\\api\\classe\\imagens\\brasilBrigada.jpg"
        },
        {
            "usuario_id": "5",
            "usuario_nome": "Gabriel Rollim1",
            "usuario_email": "gabriel.rollim@gmail.com1",
            "usuario_senha": "gabriel1232",
            "usuario_imgUrl": ""
        }
    ]
}


--===

## alterar ##

altera nome, email e senha via PUT

(PUT) http://localhost/pushstart_test/src/api/usuario/alterar/23e73e07004ed54f362e2885a5dff8ec/5

Parâmetros:

* Chave de acesso

* Parametro: usuario_id para identificação do usuário da alteração


Exemplo de envio:
{
    "usuario_nome": "Gabriel Rollim1",
    "usuario_email": "gabriel.rollim@gmail.com1",
    "usuario_senha": "gabriel1232"

}


Exemplo de retorno:
{
    "status": "sucesso",
    "dados": "dados alterados com sucesso."
}


--===

## alterar imagem ##

altera imagem via form-data (não consegui via json). *pensei em utilizar base64 com uma interface para fazer a conversão, mas fugia da premissa de que as imagens deveriam ser enviadas a um diretório).*

(POST) http://localhost/pushstart_test/src/api/usuario/alterar_imagem/799c3d0f35e552ce46f331be11f22811/4

* Chave de acesso

* Parametro: usuario_id para identificação do usuário da imagem


Exemplo de retorno:
{
    "status": "sucesso",
    "dados": "imagem alterada com sucesso."
}


Teste via POSTMAN na opção form-data passando uma "key" do tipo "File" e selecionando o arquivo no "Value".


--===

## logout ##

invalida a sessão pela chave de acesso informada na URL (atualiza a data de expiração, um campo de controle na validação da chave, para data atual).

(DELETE) http://localhost/pushstart_test/src/api/sessao/logout/a67a6aa3b1ef6a293095159070292605/

* Chave de acesso

Exemplo de retorno:
{
    "status": "sucesso",
    "dados": "sessão encerrada."
}


--==

Foram usados:

-XAMPP

-ATOM

-PHPMyAdmin

-POSTMAN

-GIT


*não foi feito HTML para testes
