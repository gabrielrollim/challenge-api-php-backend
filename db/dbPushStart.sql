-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 25-Nov-2019 às 01:58
-- Versão do servidor: 10.4.8-MariaDB
-- versão do PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Banco de dados: `dbpushstart`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `sessao`
--

CREATE TABLE `sessao` (
  `sessao_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `sessao_chave` varchar(50) NOT NULL,
  `sessao_dataexpiracao` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `sessao`
--

INSERT INTO `sessao` (`sessao_id`, `usuario_id`, `sessao_chave`, `sessao_dataexpiracao`) VALUES
(1, 5, '7a1c2ff1673bd8d6f0ec0065ca02a42e', '2019-11-24 20:32:39'),
(2, 5, 'cbdf1cc1aea0cbfed81db71bf911de8b', '2019-11-24 20:33:27'),
(3, 5, 'b899ae0744dfc84825039c1a6c46e424', '2019-11-24 20:45:41'),
(4, 5, '0bd7273414d802ecfc37b64684015411', '2019-11-24 20:48:36'),
(5, 5, '59ea0b39647ee2dbea7759c89c06799a', '2019-11-24 20:55:09'),
(6, 5, '936c23baa2001a124012699c2f268abb', '2019-11-24 21:02:59'),
(7, 5, '41f3da959e380d70c9eee9f0c205316c', '2019-11-24 21:08:20'),
(8, 5, 'aa0729a0aec22d5983cb1ef4bf7a60b5', '2019-11-24 21:20:28'),
(9, 5, 'a42868e1f069742e3543b93d22123d98', '2019-11-24 21:30:31'),
(10, 5, 'dd834492b7599872f783bd031b4ba6b9', '2019-11-24 21:35:21'),
(11, 5, '6e6596d7b75f2623ddd3399052ab1867', '2019-11-24 21:45:09'),
(12, 5, 'a789d24568511299309afa236e69c718', '2019-11-24 21:51:35'),
(13, 5, '9363b76bebb6920b9799e6aa1f6148ad', '2019-11-24 21:54:52');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `usuario_id` int(11) NOT NULL,
  `usuario_nome` varchar(100) NOT NULL,
  `usuario_email` varchar(100) DEFAULT NULL,
  `usuario_senha` varchar(100) DEFAULT NULL,
  `usuario_imgUrl` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`usuario_id`, `usuario_nome`, `usuario_email`, `usuario_senha`, `usuario_imgUrl`) VALUES
(4, 'Gabriel Rollim', 'gabriel.rollim@gmail.com', 'gabriel123', ''),
(5, 'Gabriel Rollim 22', 'gabriel.rollim2@gmail.comss', 'gabriel123', '');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `sessao`
--
ALTER TABLE `sessao`
  ADD PRIMARY KEY (`sessao_id`),
  ADD KEY `usuario_id` (`usuario_id`);

--
-- Índices para tabela `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`usuario_id`),
  ADD UNIQUE KEY `usuario_id` (`usuario_id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `sessao`
--
ALTER TABLE `sessao`
  MODIFY `sessao_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de tabela `usuario`
--
ALTER TABLE `usuario`
  MODIFY `usuario_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `sessao`
--
ALTER TABLE `sessao`
  ADD CONSTRAINT `sessao_ibfk_1` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`usuario_id`);
COMMIT;
